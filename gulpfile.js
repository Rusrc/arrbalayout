var gulp = require("gulp")
    , gulp_concat = require("gulp-concat")
    , gulp_minify_css = require("gulp-minify-css")
    , gulp_replace = require("gulp-replace")
    , gulp_less = require("gulp-less")
    , gulp_sass = require("gulp-sass")
    , gulp_watch = require("gulp-watch")
    , fs = require("fs")
    , gulpCopy = require("gulp-copy");

var tasks = [];
const pathFrom = "_Content/less";
const pathTo = "Arrba/Content";
const folderWithHtmlFiles = "Arrba";

var lessStylesArr = [
    "_Content/less/variables.less",
    "_Content/less/custom_variants.less",
    "_Content/less/font.less",
    "_Content/less/*.less",
    "_Content/less/app/*/*.less"
];

var lessStylesArrRtl = lessStylesArr.slice();
lessStylesArrRtl.unshift("_Content/less/isRtl.less");
lessStylesArr.unshift("_Content/less/isLtr.less");

var proccessLess = function (resultedFileName) {
    gulp.src(lessStylesArr)
        .pipe(gulp_concat(resultedFileName))
        .pipe(gulp_less().on('error', function (e) {
            console.log(e);
            gulp_less.end();
        }))
        //.pipe(gulp_minify_css())
        .pipe(gulp_replace(pathFrom, pathTo))
        .pipe(gulp.dest(pathTo))
        .pipe(gulp.dest("../Arrba/Content"));

    return gulp;
}

gulp.task("lessConcat", () => proccessLess("style.css"));
//gulp.task("lessConcatRtl", () => proccessLess(lessStylesArrRtl, pathTo, "style.rtl.min.css"));
tasks.push("lessConcat");

//Build html files
let htmlFiles = fs.readdirSync(folderWithHtmlFiles).filter(path => { if (/\.html$/ig.test(path)) return path; });
htmlFiles = htmlFiles.filter(e => !/profile/ig.test(e))
htmlFiles.forEach(fileName => {
    let taskName = fileName.replace(".html", "");
    let fileList = [];

    fileList.push(`${folderWithHtmlFiles}/CommonLayout/header.html`);
    if(fileName === "index_with_filter.html") {
        fileList.push(`${folderWithHtmlFiles}/CommonLayout/searchForm.html`); 
        fileList.push(`${folderWithHtmlFiles}/CommonLayout/hotSection.html`);
    }
    if(fileName === "index.html") {
        fileList.push(`${folderWithHtmlFiles}/CommonLayout/hotSection.html`);
    }
    fileList.push(`${folderWithHtmlFiles}/${fileName}`);
    fileList.push(`${folderWithHtmlFiles}/CommonLayout/footer.html`);

    gulp.task(taskName, () => {
        return gulp
            .src(fileList)
            .pipe(gulp_concat(fileName))
            .pipe(gulp.dest("./build/"));
    });

    tasks.push(taskName);
});

//Concat css files
gulp.task("concatCssFiles", function(){
    return gulp.src([
        //3.3.6"
        "node_modules/bootstrap/dist/css/bootstrap.min.css",
        "node_modules/font-awesome/css/font-awesome.min.css",
        "../Arrba/Content/style.css"
    ])
    .pipe(gulp_concat("style.min.css"))
    .pipe(gulp.dest("build"))
    .pipe(gulp.dest("../build"));
});

//Concat js files
gulp.task("concatJsFiles", function(){
    return gulp.src([
        "node_modules/jquery/dist/jquery.min.js",
        "node_modules/bootstrap/dist/js/bootstrap.min.js",
        "node_modules/masonry-layout/dist/masonry.pkgd.min.js",
        "node_modules/imagesloaded/imagesloaded.pkgd.min.js",
    ])
    .pipe(gulp_concat("script.js"))
    .pipe(gulp.dest("build"))
    .pipe(gulp.dest("../build"));
});

tasks.push("concatCssFiles");
tasks.push("concatJsFiles");
tasks.push("concatJsFiles");

//Concat css files
gulp.task("default", tasks, function () {
    //./sass/**/*.scss
    gulp.watch([
        `${folderWithHtmlFiles}/**/*.html`,
        "*.html",
        "sass/style.scss",
        "_Content/less/**/*.less"
    ],
    tasks);
});


// gulp.task('sass', function () {
//     //./sass/**/*.scss
//     gulp.src('sass/style.scss')
//         .pipe(gulp_sass({ outputStyle: 'compressed' }).on('error', gulp_sass.logError))
//         .pipe(gulp.dest('./css'));
// });

//tasks.push("sass");

